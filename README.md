Trabalho Prático – Sistemas de Informação Geográfica

Para este trabalho prático propõe-se o desenvolvimento de um plugin para o software QGIS. Este plugin será um jogo, do estilo GeoGuesser, mais rudimentar, cujo principal foco são os monumentos de Portugal. Utilizando o terminal do Python existente no QGIS, bem como as suas ferramentas, pretende-se que este jogo ajude os seus jogadores a conhecer melhor Portugal e as suas obras arquitetónicas.
Em termos de jogabilidade, o utilizador teria a capacidade de escolher se pretende adivinhar o monumento pelo seu nome ou pelo concelho/distrito/cidade onde está localizado. De seguida, conforme a sua escolha, apareceriam imagens dos monumentos, uma de cada vez, e, conforme as respostas estejam certas ou erradas, no final do jogo haveria uma pontuação atribuída ao jogador.
Pretende-se utilizar ferramentas como o Wikimedia e o Wikidata, bem como explorar as suas bases de dados e o seu conteúdo sobre Portugal para nos auxiliar no desenvolvimento deste jogo e não haver problemas com os direitos de autor das imagens.

